# What is it? #

**urbanvim** is a vim plugin that shows [UrbanDictionary](http://www.urbandictionary.com/)'s definition of any phrase you want

![example](http://i.imgur.com/hBdhC3D.png)

### How to install ###

* git clone
* cd urbanvim
* Install plugin's dependencies (pip install -r requirements.txt)
* Add path to this plugin into your .vimrc file (echo so $PWD/urban.vim >> %your_vimrc_path%)
* Reload your .vimrc file (vim -> :so $MYVIMRC)

Make sure you have vim compiled with Python support and PYTHONPATH variable set to Python directories (http://stackoverflow.com/a/35620795/1608835)

### How to use ###

* **Command mode** -- :WhatIs %term% (where %term% is a word / phrase for which you want to find a definition)
* **Visual mode** -- qq

### How can I help? ###

* Fork
* Make changes
* Create pull request

### How can I contact the author? ###

Send me an e-mail to b0r3d0mness [at] gmail [dot] com
