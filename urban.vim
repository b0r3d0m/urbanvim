" From http://stackoverflow.com/questions/1533565/how-to-get-visually-selected-text-in-vimscript/6271254#6271254
function! s:get_visual_selection()
  let [lnum1, col1] = getpos("'<")[1:2]
  let [lnum2, col2] = getpos("'>")[1:2]
  let lines = getline(lnum1, lnum2)
  let lines[-1] = lines[-1][: col2 - 1] " This line was slightly modified because the original one doesn't include the last selected character
  let lines[0] = lines[0][col1 - 1:]
  return join(lines, "\n")
endfunction

function! s:FindMeaning(word)
  if !has('python')
    echo 'Error: this plugin requires vim compiled with python support'
    finish
  endif

python << endpython

from bs4 import BeautifulSoup
import requests
import vim


try:
  word = vim.eval('a:word')

  payload = {
    'term': word
  }
  r = requests.get('http://www.urbandictionary.com/define.php', params=payload)

  soup = BeautifulSoup(r.content)
  meaning_elem = soup.find('div', class_='meaning')
  if meaning_elem is None:
    print('No definition found')
  else:
    meaning = meaning_elem.text
    print(meaning)
except Exception as ex:
  print('ERROR: {}'.format(ex))

endpython
endfunction

function! FindSelectionMeaning() range
  let l:visual_selection = s:get_visual_selection()
  call s:FindMeaning(l:visual_selection)
endfunction

command! -nargs=1 WhatIs call s:FindMeaning(<f-args>)
vnoremap <silent> qq :call FindSelectionMeaning()<CR>
